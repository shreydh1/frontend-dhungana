import { Component } from '@angular/core';
import { IdeaService } from '../idea-service/idea.service';
import { Idea } from '../idea-service/idea';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
    selector: 'idea-list',
    templateUrl: './idea-list.component.html',
    styleUrls:['./idea-list.component.css']
})
export class IdeaListComponent{
    private ideas: Idea[];
    private error: HttpErrorResponse;
    private ideaService: IdeaService;

    constructor(ideaService: IdeaService){
        this.ideaService = ideaService;
        this.ideaService.getAllIdeas().subscribe(
            ideas => this.ideas = ideas,
            error => this.error = error
        );
    }


    handleIdeaDeleted(idea: Idea){
        this.ideaService.deleteIdea(idea.ideaId).subscribe(
            idea => this.deleteIdeaFromList(idea.ideaId),
            error => this.error = error
        );
    }

    deleteIdeaFromList(ideaId: number){
        const index: number = this.ideas.findIndex(idea => idea.ideaId === ideaId);
        this.ideas.splice(index, 1);
    }
}
