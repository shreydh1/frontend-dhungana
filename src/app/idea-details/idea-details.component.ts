import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IdeaService } from '../idea-service/idea.service';
import { Idea } from '../idea-service/idea';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
    selector: 'idea-details',
    templateUrl: './idea-details.component.html',
    styleUrls: ['./idea-details.component.css']
})
export class IdeaDetailsComponent{
    private ideaId: number;
    private idea: Idea;
    private error: HttpErrorResponse;

    constructor(route: ActivatedRoute, ideaService: IdeaService){
        route.params.subscribe(params => this.ideaId = params['ideaId']);
        ideaService.getIdeaById(this.ideaId).subscribe(
            idea => this.idea = idea,
            error => this.error = error
        );
    }

}

