import { 
    Component, 
    Input,
    Output,
    EventEmitter 
} from "@angular/core";
import { Idea } from "../idea-service/idea";
import { IdeaService } from "../idea-service/idea.service";


@Component({
    selector: 'idea-card',
    templateUrl: './idea-card.component.html',
    styleUrls: ['./idea-card.component.css']
})
export class IdeaCardComponent{
    @Input()
    private idea: Idea;

    @Output()
    private ideaDeleted: EventEmitter<Idea>;

    constructor(){
        this.ideaDeleted = new EventEmitter<Idea>();
    }


    deleteIdea(){
        this.ideaDeleted.emit(this.idea);
    }

}