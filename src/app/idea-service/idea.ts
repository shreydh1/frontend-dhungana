export class Idea{
    ideaId: number;
    title: string;
    body: string;
    targetDays: number;
    goalAmount: number;
    userId: number;
    media_url:string
}