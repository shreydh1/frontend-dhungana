import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Idea } from './idea';


@Injectable()
export class IdeaService{
    private url: string = 'http://localhost:8080/api/v1/idea';
    private ideas: Idea[];
    private http: HttpClient;

    constructor(http: HttpClient){
        this.http = http;
    }

    getAllIdeas(): Observable<Idea[]>{
        return this.http.get<Idea[]>(this.url);
    }


    getIdeaById(ideaId: number): Observable<Idea>{
        return this.http.get<Idea>(`${this.url}/${ideaId}`);
    }

    createIdea(idea: Idea): Observable<Idea>{
        return this.http.post<Idea>(`${this.url}/`, idea);
    }

    updateIdea(ideaId: number, idea: Idea): Observable<Idea>{
        return this.http.put<Idea>(`${this.url}/${ideaId}`, idea);
    }

    deleteIdea(ideaId: number): Observable<Idea>{
        return this.http.delete<Idea>(`${this.url}/${ideaId}`);
    }

}