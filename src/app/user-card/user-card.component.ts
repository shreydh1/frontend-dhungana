import { 
  Component, 
  Input,
  Output,
  EventEmitter 
} from "@angular/core";
import { User } from "../user-service/user";
import { UserService } from "../user-service/user.service";

@Component({
  selector: 'user-card',
  templateUrl: './user-card.component.html',
  styleUrls: ['./user-card.component.css']
})
export class UserCardComponent {

  @Input()
  private user: User;

  @Output()
  private userDeleted: EventEmitter<User>;

  constructor(){
      this.userDeleted = new EventEmitter<User>();
  }


  deleteUser(){
      this.userDeleted.emit(this.user);
  }

}
