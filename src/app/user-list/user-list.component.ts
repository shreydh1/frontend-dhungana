import { Component, OnInit } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { User } from '../user-service/user';
import {UserService} from '../user-service/user.service'
@Component({
  selector: 'user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent {
  private users: User[];
    private error: HttpErrorResponse;
    private userService: UserService;

    constructor(userService: UserService){
        this.userService = userService;
        this.userService.getAllUsers().subscribe(
            users => this.users = users,
            error => this.error = error
        );
    }


    handleUserDeleted(user: User){
      this.userService.deleteUser(user.userId).subscribe(
          user => this.deleteUserFromList(user.userId),
          error => this.error = error
      );
  }

  deleteUserFromList(userId: number){
      const index: number = this.users.findIndex(user => user.userId === userId);
      this.users.splice(index, 1);
  }
}


