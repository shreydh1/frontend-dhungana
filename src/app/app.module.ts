import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';


import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { IdeaListComponent } from './idea-list/idea-list.component';
import { HeaderLinksComponent } from './header-links/header-links.component';
import { IdeaDetailsComponent } from './idea-details/idea-details.component';
import { IdeaFormComponent } from './idea-form/idea-form.component';
import { UserDetailsComponent } from './user-details/user-details.component';
import { componentFactoryName } from '@angular/compiler';
import { IdeaService } from './idea-service/idea.service';
import { UserService } from './user-service/user.service';
import { UserFormComponent } from './user-form/user-form.component';
import { IdeaCardComponent } from './idea-card/idea-card.component';
import { UserListComponent } from './user-list/user-list.component';
import { UserCardComponent } from './user-card/user-card.component';



@NgModule({
    declarations: [ 
        AppComponent,
        HeaderComponent,
        IdeaListComponent,
        HeaderLinksComponent,
        IdeaDetailsComponent,
        IdeaFormComponent,
        IdeaCardComponent,
        UserDetailsComponent,
        UserFormComponent,
        UserListComponent,
        UserCardComponent
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
        FormsModule,
        RouterModule.forRoot([
            {
                path: '', 
                redirectTo: 'ideas',
                pathMatch: 'full'
            },
            {
                path: 'ideas',
                component: IdeaListComponent
            },
            {
                path: 'ideas/create',
                component: IdeaFormComponent
            },
            {
                path: 'ideas/:ideaId',
                component: IdeaDetailsComponent
            },
            {
                path: 'ideas/:ideaId/update',
                component: IdeaFormComponent
            },
            
            {
                path: 'user',
                component: UserListComponent
            },
            {
                path: 'user/create',
                component: UserFormComponent
            },
            {
                path: 'user/:userId',
                component: UserDetailsComponent
            },
            {
                path: 'user/:userId/update',
                component: UserFormComponent
            }
           
           
        ])
    ],
    providers: [
        IdeaService,
        UserService
    ],
    bootstrap: [ AppComponent ]
 })
 export class AppModule { }