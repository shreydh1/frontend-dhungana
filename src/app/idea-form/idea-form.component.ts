import { Component } from '@angular/core';
import { Idea } from '../idea-service/idea';
import { IdeaService } from '../idea-service/idea.service';
import { HttpErrorResponse } from '@angular/common/http';
import { 
    Router,
    ActivatedRoute 
} from '@angular/router';

@Component({
    selector: 'idea-form',
    templateUrl: './idea-form.component.html',
    styleUrls: ['./idea-form.component.css']
})
export class IdeaFormComponent{
    private ideaId: number;
    private idea: Idea;
    private ideaService: IdeaService;
    private error: HttpErrorResponse;
    private router: Router;

    constructor(router: Router, route: ActivatedRoute, ideaService: IdeaService){
        this.ideaService = ideaService;
        this.router = router;
        route.params.subscribe(params => this.ideaId = params['ideaId']);
        if(this.ideaId){
            ideaService.getIdeaById(this.ideaId).subscribe(
                idea => this.idea = idea,
                error => this.error = error
            );
        } else {
            this.idea = new Idea();
        }
    }

    submitIdea(){
        if(this.ideaId){
            this.ideaService.updateIdea(this.ideaId, this.idea).subscribe(
                idea => this.router.navigate(['ideas', this.ideaId]),
                error => this.error = error
            );
        } else {
            this.ideaService.createIdea(this.idea).subscribe(
                idea => this.router.navigate(['ideas']),
                error => this.error = error
            );
        }
    }

}